﻿using DotNetCommunity_ChainOfResponsability.Models;

namespace DotNetCommunity_ChainOfResponsability.Contexts
{
    public class Context
    {
        public Client Client { get; set; }
    }
}
