﻿using DotNetCommunity_ChainOfResponsability.Chains;
using DotNetCommunity_ChainOfResponsability.Contexts;
using DotNetCommunity_ChainOfResponsability.Predicates;
using NeonSource.Patterns.ChainOfResponsability.Builders;
using System;

namespace DotNetCommunity_ChainOfResponsability
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Montando chain");

            var chain = new ChainBuilder<Context>()
                .Then(new GetClientChain())
                .Then(new PredicateBuilder<Context>()
                    .Predicate(new IsDebtorClientPredicate())
                    .True(new TreatDebtorClientChain())
                    .False(new TreatNotDebtorClientChain())
                    .Build()
                )
                .Then(new TreatClientChain())
                .Build();

            var result = chain.Execute(new Context()).GetAwaiter().GetResult();

            Console.WriteLine("Chain executada com sucesso!");

            Console.ReadKey();
        }
    }
}
