﻿using DotNetCommunity_ChainOfResponsability.Contexts;
using NeonSource.Patterns.ChainOfResponsability.Abstractions;

namespace DotNetCommunity_ChainOfResponsability.Predicates
{
    public class IsDebtorClientPredicate : Predicate<Context>
    {
        public override bool Validate(Context chainParam)
        {
            return chainParam.Client.Debt;
        }
    }
}
