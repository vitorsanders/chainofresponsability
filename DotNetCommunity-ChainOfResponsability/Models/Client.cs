﻿namespace DotNetCommunity_ChainOfResponsability.Models
{
    public class Client
    {
        public string Name { get; set; }
        public bool Debt { get; set; }
    }
}
