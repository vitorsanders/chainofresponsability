﻿using DotNetCommunity_ChainOfResponsability.Contexts;
using NeonSource.Patterns.ChainOfResponsability.Abstractions;
using System;
using System.Threading.Tasks;

namespace DotNetCommunity_ChainOfResponsability.Chains
{
    public class TreatDebtorClientChain : Chain<Context>
    {
        public override Task<Context> Execution(Context chainParam)
        {
            Console.WriteLine("Cliente Em Débito");
            return Task.FromResult(chainParam);
        }
    }
}
