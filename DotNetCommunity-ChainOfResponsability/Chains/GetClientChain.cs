﻿using Bogus;
using DotNetCommunity_ChainOfResponsability.Contexts;
using DotNetCommunity_ChainOfResponsability.Models;
using NeonSource.Patterns.ChainOfResponsability.Abstractions;
using System;
using System.Threading.Tasks;

namespace DotNetCommunity_ChainOfResponsability.Chains
{
    public class GetClientChain : Chain<Context>
    {
        private Faker _faker;
        public GetClientChain()
        {
            _faker = new Faker("pt_BR");
        }
        public override Task<Context> Execution(Context chainParam)
        {
            chainParam.Client = new Client()
            {
                Name = _faker.Person.FullName,
                Debt = false
            };
            return Task.FromResult(chainParam);
        }
    }
}
