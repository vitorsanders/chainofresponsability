﻿using DotNetCommunity_ChainOfResponsability.Contexts;
using NeonSource.Patterns.ChainOfResponsability.Abstractions;
using System;
using System.Threading.Tasks;

namespace DotNetCommunity_ChainOfResponsability.Chains
{
    public class TreatClientChain : Chain<Context>
    {
        public override Task<Context> Execution(Context chainParam)
        {
            Console.WriteLine($"Nome do Cliente: {chainParam.Client.Name}");
            return Task.FromResult(chainParam);
        }
    }
}
