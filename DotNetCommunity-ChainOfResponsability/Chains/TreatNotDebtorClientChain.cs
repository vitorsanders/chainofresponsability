﻿using DotNetCommunity_ChainOfResponsability.Contexts;
using NeonSource.Patterns.ChainOfResponsability.Abstractions;
using System;
using System.Threading.Tasks;

namespace DotNetCommunity_ChainOfResponsability.Chains
{
    public class TreatNotDebtorClientChain : Chain<Context>
    {
        public override Task<Context> Execution(Context chainParam)
        {
            Console.WriteLine("Cliente Não está Em Débito");
            return Task.FromResult(chainParam);
        }
    }
}
